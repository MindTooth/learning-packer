source "qemu" "opensuse" {
  vm_name = "qemu-opensuse"
  iso_url = "https://download.opensuse.org/distribution/leap/15.2/iso/openSUSE-Leap-15.2-NET-x86_64.iso"
  iso_checksum = "sha256:02a77812443dd3e47c36611c8837bf8d1659aea6b4d16c9db6841c564d59b7af"
  output_directory = "build"
  shutdown_command = "echo 'packer' | sudo -S shutdown -P now"
  cpus = 4
  memory = 2048
  disk_size = "25G"
  format = "qcow2"
  headless = true
  http_directory = "seed"
  pause_before_connecting = "15m"
  ssh_handshake_attempts = "120"
  ssh_keep_alive_interval = "1m"
  ssh_password = "packer-user"
  ssh_timeout = "90m"
  ssh_username = "packer"
  vnc_port_max = 5957
  vnc_port_min = 5957
  boot_wait = "15s"
  boot_command = ["<down>", "autoyast=http://{{ .HTTPIP }}:{{ .HTTPPort }}/opensuse.xml ", "textmode=1 ", "<enter>"]
}

build {
  sources = ["source.qemu.opensuse"]
}
